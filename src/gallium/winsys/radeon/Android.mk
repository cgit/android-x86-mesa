ifeq ($(strip $(MESA_BUILD_R300)),true)

LOCAL_PATH := $(call my-dir)

# from drm/Makefile
C_SOURCES = \
	radeon_drm_bo.c \
	radeon_drm_common.c \
	radeon_drm_cs.c

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	$(addprefix drm/, $(C_SOURCES))

LOCAL_CFLAGS := \
	-std=c99 \
	-fvisibility=hidden \
	-Wno-sign-compare

LOCAL_C_INCLUDES := \
	external/mesa/include \
	external/mesa/src/gallium/include \
	external/mesa/src/gallium/auxiliary \
	external/mesa/src/gallium/drivers/r300 \
	external/drm \
	external/drm/include/drm

LOCAL_MODULE := libmesa_winsys_r300

include $(BUILD_STATIC_LIBRARY)

endif # MESA_BUILD_R300
