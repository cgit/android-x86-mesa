LLVMPIPE -- a fork of softpipe that employs LLVM for code generation.


Requirements
============

 - A x86 or amd64 processor.  64bit mode is preferred.
 
   Support for sse2 is strongly encouraged.  Support for ssse3, and sse4.1 will
   yield the most efficient code.  The less features the CPU has the more
   likely is that you ran into underperforming, buggy, or incomplete code.  
   
   See /proc/cpuinfo to know what your CPU supports.
 
 - LLVM. Version 2.8 recommended. 2.6 or later required.
 
   For Linux, on a recent Debian based distribution do:
 
     aptitude install llvm-dev

   For Windows download pre-built MSVC 9.0 or MinGW binaries from
   http://people.freedesktop.org/~jrfonseca/llvm/ and set the LLVM environment
   variable to the extracted path.

   For MSVC there are two set of binaries: llvm-x.x-msvc32mt.7z and
   llvm-x.x-msvc32mtd.7z .

   You have to set the LLVM=/path/to/llvm-x.x-msvc32mtd env var when passing
   debug=yes to scons, and LLVM=/path/to/llvm-x.x-msvc32mt when building with
   debug=no. This is necessary as LLVM builds as static library so the chosen
   MS CRT must match.

 - scons (optional)


Building
========

To build everything on Linux invoke scons as:

  scons build=debug libgl-xlib

Alternatively, you can build it with GNU make, if you prefer, by invoking it as

  make linux-llvm

but the rest of these instructions assume that scons is used.

For windows is everything the except except the winsys:

  scons build=debug libgl-gdi

Using
=====

On Linux, building will create a drop-in alternative for libGL.so into

  build/foo/gallium/targets/libgl-xlib/libGL.so

To use it set the LD_LIBRARY_PATH environment variable accordingly.

For performance evaluation pass debug=no to scons, and use the corresponding
lib directory without the "-debug" suffix.

On Windows, building will create a drop-in alternative for opengl32.dll. To use
it put it in the same directory as the application. It can also be used by
replacing the native ICD driver, but it's quite an advanced usage, so if you
need to ask, don't even try it.


Profiling
=========

To profile llvmpipe you should pass the options

  scons build=profile <same-as-before>

This will ensure that frame pointers are used both in C and JIT functions, and
that no tail call optimizations are done by gcc.


To better profile JIT code you'll need to build LLVM with oprofile integration.

  ./configure \
      --prefix=$install_dir \
      --enable-optimized \
      --disable-profiling \
      --enable-targets=host-only \
      --with-oprofile

  make -C "$build_dir"
  make -C "$build_dir" install

  find "$install_dir/lib" -iname '*.a' -print0 | xargs -0 strip --strip-debug

The you should define

  export LLVM=/path/to/llvm-2.6-profile

and rebuild.


Unit testing
============

Building will also create several unit tests in
build/linux-???-debug/gallium/drivers/llvmpipe:

 - lp_test_blend: blending
 - lp_test_conv: SIMD vector conversion
 - lp_test_format: pixel unpacking/packing

Some of this tests can output results and benchmarks to a tab-separated-file
for posterior analysis, e.g.:

  build/linux-x86_64-debug/gallium/drivers/llvmpipe/lp_test_blend -o blend.tsv


Development Notes
=================

- When looking to this code by the first time start in lp_state_fs.c, and 
  then skim through the lp_bld_* functions called in there, and the comments
  at the top of the lp_bld_*.c functions.  

- The driver-independent parts of the LLVM / Gallium code are found in
  src/gallium/auxiliary/gallivm/.  The filenames and function prefixes
  need to be renamed from "lp_bld_" to something else though.

- We use LLVM-C bindings for now. They are not documented, but follow the C++
  interfaces very closely, and appear to be complete enough for code
  generation. See 
  http://npcontemplation.blogspot.com/2008/06/secret-of-llvm-c-bindings.html
  for a stand-alone example.  See the llvm-c/Core.h file for reference.
