ifeq ($(strip $(MESA_BUILD_R300)),true)

LOCAL_PATH := $(call my-dir)

# from Makefile
C_SOURCES = \
	r300_blit.c \
	r300_chipset.c \
	r300_context.c \
	r300_debug.c \
	r300_emit.c \
	r300_flush.c \
	r300_fs.c \
	r300_hyperz.c \
	r300_query.c \
	r300_render.c \
	r300_render_stencilref.c \
	r300_render_translate.c \
	r300_resource.c \
	r300_screen.c \
	r300_screen_buffer.c \
	r300_state.c \
	r300_state_derived.c \
	r300_vs.c \
	r300_vs_draw.c \
	r300_texture.c \
	r300_texture_desc.c \
	r300_tgsi_to_rc.c \
	r300_transfer.c

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	$(C_SOURCES)

LOCAL_CFLAGS := \
	-std=c99 \
	-fvisibility=hidden \
	-Wno-sign-compare

LOCAL_C_INCLUDES := \
	external/mesa/src/gallium/include \
	external/mesa/src/gallium/auxiliary \
	external/drm \
	external/drm/include/drm \
	external/mesa/src/mesa/drivers/dri/r300/compiler \
	external/mesa/gallium/winsys/drm/radeon/core

LOCAL_MODULE := libmesa_pipe_r300

include $(BUILD_STATIC_LIBRARY)

endif # MESA_BUILD_R300
